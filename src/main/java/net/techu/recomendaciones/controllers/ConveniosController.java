package net.techu.recomendaciones.controllers;


import net.techu.recomendaciones.models.Condiciones;
import net.techu.recomendaciones.models.ConveniosModel;
import net.techu.recomendaciones.models.ConveniosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ConveniosController {
    
    @Autowired
    private ConveniosRepository repository;


    /*Get para lista de Convenios*/
    @GetMapping(value = "/convenios", produces = "application/json")
    public ResponseEntity<List<ConveniosModel>> listaConvenios(){
        List<ConveniosModel> convenio = repository.findAll();
        return new ResponseEntity<List<ConveniosModel>>(convenio, HttpStatus.OK);
    }

    /*Get para Convenios por id*/
    @GetMapping(value = "/convenios/{id}", produces = "application/json")
    public ResponseEntity<ConveniosModel> getConvenio(@PathVariable String id){
        Optional<ConveniosModel> convenio = repository.findById(id);
        return new ResponseEntity(convenio, HttpStatus.OK);
    }

    /*Post para registrar Convenios */
    @PostMapping(value = "/convenios")
    public ResponseEntity<String> crearConvenio(@RequestBody ConveniosModel newConvenio){
        ConveniosModel addCliente = repository.insert(newConvenio);
        return new ResponseEntity<>("Convenio creado correctamente", HttpStatus.CREATED);
    }


    /*Put para modificar las condiciones de Convenios por id*/
    @PatchMapping(value = "/convenios/{nombre}/{id}")
    public ResponseEntity<String> updateConvenio(@RequestBody Condiciones dataCondicion, @PathVariable String id, @PathVariable String nombre ){
        ConveniosModel consulta = repository.findByName(nombre);
        int indice = 0;
        if (consulta != null){
            List<Condiciones>  condiciones = consulta.condiciones;
            for(Condiciones condicion: condiciones ){
                if(condicion.getId().compareTo(id) == 0){
                    condicion.producto = dataCondicion.producto;
                    condicion.promocion= dataCondicion.promocion;
                    consulta.condiciones.set(indice,condicion);
                    repository.save(consulta);
                    return new ResponseEntity<>("convenio modificado", HttpStatus.OK);
                }
                indice++;
            }
        }
        return new ResponseEntity<>("No se ha encontrado el convenio", HttpStatus.NOT_FOUND);
    }

    /*Put para modificar las condiciones de Convenios por id*/
    @PutMapping(value = "/convenios/{nombre}")
    public ResponseEntity<String> addCondiciones(@RequestBody Condiciones dataCondicion, @PathVariable String nombre ){
        ConveniosModel consulta = repository.findByName(nombre);
        if (consulta != null){
            consulta.condiciones.add(dataCondicion);
            repository.save(consulta);
            return new ResponseEntity<>("Condición agregada al convenio", HttpStatus.CREATED);
        }
        return new ResponseEntity<>("No se ha encontrado el convenio", HttpStatus.NOT_FOUND);
    }


    /*Delete de Convenios por id*/
    @DeleteMapping("/convenios/{id}")
    public ResponseEntity<String> deleteCliente(@PathVariable String id){
        repository.deleteById(id);

        return new ResponseEntity<>("Cliente eliminado Correctamente", HttpStatus.OK);
    }
}
