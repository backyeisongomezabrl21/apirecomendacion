package net.techu.recomendaciones.controllers;

import net.techu.recomendaciones.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@RestController
public class SugerenciasController {

    @Autowired
    private ConveniosRepository repository;

    @PostMapping("/sugerencias")
    public ResponseEntity<Sugerencias> sugerencia(@RequestBody ConvenioCliente convenioCliente){

        ArrayList<Condiciones> promociones = new ArrayList<>() ;

        RestTemplate template = new RestTemplate();
        Clientes cliente = template.getForObject("http://localhost:8080/v1/clientesbyid/" + convenioCliente.idCliente , Clientes.class); //consulta un cliente por id de la api de clientes

        if(cliente != null) {
            List<Productos> productos = cliente.getProductos();  //los productos relacionados a un cliente

            ConveniosModel convenio = repository.findByName(convenioCliente.nombreConvenio); //el convenio consultado por el cliente

            if (convenio != null) {
                List<Condiciones> condiciones = convenio.condiciones; //condiciones del convenio dado por el cliente

                for (Condiciones condicion : condiciones) {
                    for (Productos producto : productos) {
                        if (condicion.producto.compareToIgnoreCase(producto.getNombreProducto()) == 0) {
                            promociones.add(condicion);
                        }
                    }
                }
                Sugerencias sugerencias = new Sugerencias(convenioCliente.nombreConvenio, promociones);

                Recomendacion recomendacion = new Recomendacion(convenioCliente.idCliente, sugerencias.nombreConvenio, sugerencias.promocion);

                template.postForObject("http://localhost:6060/kafka/recomendacion",recomendacion, Recomendacion.class);


                return new ResponseEntity<>(sugerencias, HttpStatus.ACCEPTED);
            }
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);

    }
}
