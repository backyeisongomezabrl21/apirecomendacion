package net.techu.recomendaciones.models;

import java.util.List;

public class Sugerencias {

    public String nombreConvenio;
    public List<Condiciones> promocion;

    public Sugerencias(String nombreConvenio, List<Condiciones> promocion) {
        this.nombreConvenio = nombreConvenio;
        this.promocion = promocion;
    }

    public String getNombreConvenio() {
        return nombreConvenio;
    }

    public void setNombreConvenio(String nombreConvenio) {
        this.nombreConvenio = nombreConvenio;
    }

    public List<Condiciones> getPromocion() {
        return promocion;
    }

    public void setPromocion(List<Condiciones> promocion) {
        this.promocion = promocion;
    }

}
