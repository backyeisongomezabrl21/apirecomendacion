package net.techu.recomendaciones.models;

import org.springframework.data.mongodb.core.mapping.Document;

@Document("condiciones")
public class Condiciones {

    public String id;
    public String producto;
    public String promocion;


    public Condiciones(String id, String producto, String promocion) {
        this.id = id;
        this.producto = producto;
        this.promocion = promocion;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getPromocion() {
        return promocion;
    }

    public void setPromocion(String promocion) {
        this.promocion = promocion;
    }
}
