package net.techu.recomendaciones.models;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document("convenios")
public class ConveniosModel {

    public String id;
    public String nombre;
    public List<Condiciones> condiciones;

    public ConveniosModel(String id, String nombre, List<Condiciones> condiciones) {
        this.id = id;
        this.nombre = nombre;
        this.condiciones = condiciones;
    }
}
