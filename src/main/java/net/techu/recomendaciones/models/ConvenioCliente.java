package net.techu.recomendaciones.models;

import org.springframework.data.mongodb.core.mapping.Document;


public class ConvenioCliente {

    public String idCliente;
    public String nombreConvenio;

    public ConvenioCliente(String idClinte, String nombreConvenio) {
        this.idCliente = idClinte;
        this.nombreConvenio = nombreConvenio;
    }

    public String getIdClinte() {
        return idCliente;
    }

    public void setIdClinte(String idClinte) {
        this.idCliente = idClinte;
    }

    public String getNombreConvenio() {
        return nombreConvenio;
    }

    public void setNombreConvenio(String nombreConvenio) {
        this.nombreConvenio = nombreConvenio;
    }
}
