package net.techu.recomendaciones.models;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface ConveniosRepository extends MongoRepository<ConveniosModel,String> {

    @Query("{'nombre':?0}")
    public ConveniosModel findByName(String nombre);

    @Query("{'condiciones':?0}")
    public Condiciones findCondicionById(String id);

}
