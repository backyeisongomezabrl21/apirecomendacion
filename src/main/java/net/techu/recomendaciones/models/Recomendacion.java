package net.techu.recomendaciones.models;

import java.util.List;

public class Recomendacion {

    public String idCliente;
    public String nombreConvenio;
    public List<Condiciones> promocions;

    public Recomendacion(String idCliente, String nombreConvenio, List<Condiciones> promocions) {
        this.idCliente = idCliente;
        this.nombreConvenio = nombreConvenio;
        this.promocions = promocions;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombreConvenio() {
        return nombreConvenio;
    }

    public void setNombreConvenio(String nombreConvenio) {
        this.nombreConvenio = nombreConvenio;
    }

    public List<Condiciones> getPromocions() {
        return promocions;
    }

    public void setPromocions(List<Condiciones> promocions) {
        this.promocions = promocions;
    }
}
